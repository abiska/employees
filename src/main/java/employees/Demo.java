package employees;

import java.util.Date;

/**
 *
 * @author iuabd
 */
public class Demo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //object(s)
        //Calendar dateJoined;
        Date d = new Date();
        Employee emp = new Employee("100", "Sherlock", d);
        
        //Tool object
        Tool tool = new Tool();
        
        //Print
        System.out.println("ID: "+emp.getEmployeeID() +"\nName: "+emp.getName() +"\nDate of joining: "+emp.getDateOfJoining());
        System.out.println("Income tax: "+tool.calcIncomeTaxForCurrentYear(1000));
        System.out.println("Promotion this year? "+tool.isPromotionDueThisYear(true));
        
    }
    
}
