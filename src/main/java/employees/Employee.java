package employees;

import java.util.Date;

/**
 *
 * @author iuabd
 */
public class Employee {
    
    //attribute(s)
    private String employeeID;
    private String name;
    private Date dateOfJoining;
    
    //getter(s)
    public String getEmployeeID() {
        return employeeID;
    }
    public String getName() {
        return name;
    }
    public Date getDateOfJoining() {
        return dateOfJoining;
    }
    
    //constructor(s)
    public Employee(){};
    public Employee(String employeeID, String name, Date dateOfJoining) {
        this.employeeID = employeeID;
        this.name = name;
        this.dateOfJoining = dateOfJoining;
    }
    
}
